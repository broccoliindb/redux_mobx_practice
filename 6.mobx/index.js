const { observable, autorun, reaction, runInAction, action } = require('mobx')

const state = observable({
  compA: 'a',
  compB: 12,
  compC: null
})

autorun(() => {
  console.log('changed', state.compA)
})

const change = action(() => {
  state.compA = 'b'
  state.compB = 'c'
  state.compA = 'd'
})

runInAction(() => {
  state.compA = 'b'
  state.compA = 'c'
  state.compA = 'd'
})

runInAction(() => {
  state.compA = '1'
  state.compA = '2'
  state.compA = '3'
})

reaction(
  () => {
    return state.compB
  },
  () => {
    console.log('reaction', state.compB)
  }
)

change()
