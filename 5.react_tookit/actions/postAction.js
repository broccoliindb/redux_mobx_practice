const { createAsyncThunk } = require('@reduxjs/toolkit')

const delay = (time, value) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value)
    }, time)
  })
}

const addPost = createAsyncThunk('post/add', async (data, thunkApi) => {
  const result = await delay(2000, {
    title: '새게시글',
    content: '새글....'
  })
  return result
})

module.exports = {
  addPost
}
