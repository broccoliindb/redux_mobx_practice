const { createAsyncThunk } = require('@reduxjs/toolkit')

const delay = (time, value) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value)
    }, time)
  })
}

const login = createAsyncThunk('user/login', async (data, thunkApi) => {
  // const state = thunkApi.getState() 이렇게 스테이트를 받을 수도 있다.
  // data 는 원래 이메일이나 패스워드로 데이터 받고 결과를 받는것인데..일단 가짜 데이터 리턴 받자
  // 여기서는 try catch는 하지 않아야 rejected시 응답을 받을 수 있다.
  console.log('login data: ', data)
  const result = await delay(2000, {
    userId: 1,
    name: 'cyb'
  })
  return result
})

module.exports = {
  login
}
