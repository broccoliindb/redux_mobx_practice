const { createSlice } = require('@reduxjs/toolkit')
const { addPost } = require('../actions/postAction')

const initialState = {
  data: []
}

// reducer는 동기적인 액션, 내부적인 액션을 넣는다.
// extraReducer는 비동기적인 액션, 내부를 벗어나는 범위의 액션을 넣는다.

const postSlice = createSlice({
  name: 'post',
  initialState,
  reducers: {
    clearPost: (state, action) => {
      state.data = []
    }
  },
  extraReducers: (builder) =>
    builder
      .addCase(addPost.pending, (state, action) => {})
      .addCase(addPost.fulfilled, (state, action) => {})
      .addCase(addPost.rejected, (state, action) => {})
      .addMatcher(
        // matcher can be defined inline as a type predicate function
        (action) => action.type.endsWith('/rejected'),
        (state, action) => {
          state[action.meta.requestId] = 'rejected'
        }
      )
      .addDefaultCase((state, action) => {})

  // [addPost.pening]: (state, action) => {},
  // [addPost.fulfilled]: (state, action) => {
  //   state.data.push(action.payload)
  // },
  // [addPost.rejected]: (state, action) => {}
})

module.exports = postSlice
