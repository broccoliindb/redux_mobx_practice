const { createSlice } = require('@reduxjs/toolkit')
const { login } = require('../actions/userAction')

const initialState = {
  isLoggingIn: false,
  data: null
}

// ES6 Dynamic Property Keys
// let cake = '🍰';
// let pan = {
//   [cake]: '🥞',
// };
// Output -> { '🍰': '🥞' }

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout(state, action) {
      state.isLoggingIn = false
      state.data = null
    }
  },
  extraReducers: {
    // user/login/pending 이라는 이름의 액션이 생김
    [login.pending]: (state, action) => {
      state.isLoggingIn = true
    },
    // user/login/fulfilled 이라는 이름의 액션이 생김
    [login.fulfilled]: (state, action) => {
      state.data = action.payload
      state.isLoggingIn = true
    },
    // user/login/rejected 이라는 이름의 액션이 생김
    [login.rejected]: (state, action) => {
      state.data = null
      state.isLoggingIn = false
    }
  }
})

module.exports = userSlice
