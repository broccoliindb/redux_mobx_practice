const { combineReducers } = require('redux')
const userSlice = require('./userSlice')
const postSlice = require('./postSlice')

// slice는 리듀서도 있고, 액션도 있고, 초기 스테이트도 있음. 즉 여러가지가 합쳐져 있는것. 합쳐진 이유는 한 세트로 어떤 액션은 특정 리듀서에 종속적으로 묶이는 경우가 많아서.

module.exports = combineReducers({
  user: userSlice.reducer,
  posts: postSlice.reducer
})
