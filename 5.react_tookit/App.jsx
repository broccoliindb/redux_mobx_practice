import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
const { login } = require('./actions/userAction')
const { addPost } = require('./actions/postAction')
const userSlice = require('./slices/userSlice')
const App = () => {
  const isLoggingIn = useSelector((state) => state.user.isLoggingIn)
  const user = useSelector((state) => state.user.data)
  console.log('user', user)
  const dispatch = useDispatch()
  const loginHandler = useCallback(() => {
    dispatch(
      login({
        id: 'asdgasfcasdf',
        password: '112341'
      })
    )
  }, [])
  const logoutHandler = useCallback(() => {
    dispatch(userSlice.actions.logout())
  }, [])
  const addPostHandler = useCallback(() => {
    dispatch(
      addPost({
        id: '1',
        content: '새글이요'
      })
    )
  }, [])
  return (
    <div>
      <button onClick={isLoggingIn ? logoutHandler : loginHandler}>
        {isLoggingIn ? '로그아웃' : '로그인'}
      </button>
      {isLoggingIn
        ? user && user.name
          ? `${user.name}환영`
          : '로그인중입니다.'
        : '로그인해주세요'}
      <button onClick={addPostHandler}>글올리기</button>
    </div>
  )
}

export default App
