const reducer = require('./slices')
const { configureStore } = require('@reduxjs/toolkit')

const testMiddleWare = (store) => (next) => (action) => {
  console.group('로깅', action)
  next(action)
}

// preloadedState: initialState와 같은데, 서버사이드 렌더링 전용. 디폴트로 일반적인 initalState는 알아서 생김.
const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(testMiddleWare), // 미들웨어 추가하면 디폴트미들웨어가 동작을 안하므로 추가해야한다. 여기에 thunk가 있다.
  devTools: process.env.NODE_ENV !== 'production'
})

module.exports = store
