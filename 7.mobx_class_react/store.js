import { observable } from 'mobx'

export const userStore = observable({
  isLoggingIn: false,
  isLogged: false,
  data: null,
  login(data) {
    console.log(data)
    this.isLoggingIn = true

    setTimeout(() => {
      this.data = { name: '용범이' }
      this.isLoggingIn = false
      this.isLogged = true
      postStore.posts.push(1)
    }, 2000)
  },
  logout() {
    this.data = null
    this.isLogged = false
    this.isLoggingIn = false
  }
})
export const postStore = observable({
  posts: [],
  addPost: (data) => {
    this.posts.push(data)
  }
})
