import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { userStore, postStore } from './store'

class App extends Component {
  state = observable({
    userId: '',
    password: ''
  })

  onLogin = (event) => {
    event.preventDefault()
    const data = {
      userId: this.state.userId,
      password: this.state.password
    }
    userStore.login(data)
  }
  onLogout = () => {
    event.preventDefault()
    userStore.logout()
    this.state.userId = ''
    this.state.password = ''
  }
  onChangeId = (event) => {
    this.state.userId = event.target.value
    console.log(this.state.userId)
  }
  onChangePassword = (event) => {
    this.state.password = event.target.value
    console.log(this.state.password)
  }
  render() {
    return (
      <>
        <form onSubmit={userStore.data ? this.onLogout : this.onLogin}>
          <input
            type="text"
            value={this.state.userId}
            placeholder="userId"
            onChange={this.onChangeId}
          />
          <input
            type="password"
            value={this.state.password}
            placeholder="password"
            onChange={this.onChangePassword}
          />
          <button>{userStore.data ? '로그아웃' : '로그인'}</button>
        </form>
        {userStore.isLoggingIn
          ? '로그인중입니다.'
          : userStore.isLogged
          ? `${userStore.data.name}환영합니다.`
          : '로그인해주세요'}
        {postStore.posts.length > 0 && `게사판 글수:${postStore.posts.length}`}
      </>
    )
  }
}

export default observer(App)
