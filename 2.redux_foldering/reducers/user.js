const initialState = {
  isLoggingIn: false,
  data: null
}
const userReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...prevState,
        isLoggingIn: true,
        data: action.data
      }
    case 'LOGOUT':
      return {
        ...prevState,
        isLoggingIn: false,
        data: null
      }
    case 'LOGIN_REQUEST':
      return {
        ...prevState,
        data: action.data
      }
    case 'lOGIN_SUCCESS':
      return {
        ...prevState,
        data: action.data
      }
    default:
      return prevState
  }
}

module.exports = userReducer
