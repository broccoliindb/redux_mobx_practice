const reducer = require('./reducers')
const { login } = require('./actions/user')
// const { addpost } = require('./actions/post')

const { createStore, applyMiddleware, compose } = require('redux')

const initState = {
  user: {
    isLoggingIn: false,
    data: null
  },
  posts: [],
  comments: [],
  favorites: [],
  hisotry: [],
  likes: [],
  followers: []
}

const testMiddleWare = (store) => (next) => (action) => {
  next(action)
}

const thunkMiddleware = (store) => (next) => (action) => {
  console.log('미드', action)
  if (typeof action === 'function') {
    return action(store.dispatch, store.getState)
  } else {
    return next(action)
  }
}

const enhancer = compose(applyMiddleware(testMiddleWare, thunkMiddleware))

const store = createStore(reducer, initState, enhancer)
store.subscribe(() => {
  console.log('change', store.getState())
})
console.log(store.getState())
store.dispatch(
  login({
    id: 1,
    user: 'a',
    password: '1111',
    admin: true
  })
)
// store.dispatch(addpost({ id: 1, userId: 1, content: 'first post' }))
// store.dispatch(addpost({ id: 2, userId: 1, content: 'second post' }))
// store.dispatch(logout())
