import React, { useCallback } from 'react'
import { useObserver, useLocalObservable } from 'mobx-react'
import { userStore, postStore } from './store'

const App = () => {
  const state = useLocalObservable(() => ({
    userId: '',
    password: ''
  }))
  const onLogin = useCallback((e) => {
    e.preventDefault()
    const data = {
      userId: state.userId,
      password: state.password
    }
    userStore.login(data)
  }, [])
  const onLogout = useCallback((e) => {
    e.preventDefault()
    userStore.logout()
    state.userId = ''
    state.password = ''
  }, [])
  const onChangeId = useCallback((e) => {
    state.userId = e.target.value
    console.log(state.userId)
  }, [])
  const onChangePassword = useCallback((e) => {
    state.password = e.target.value
    console.log(state.password)
  }, [])
  return useObserver(() => (
    <>
      <form onSubmit={userStore.data ? onLogout : onLogin}>
        <input
          type="text"
          value={state.userId}
          placeholder="userId"
          onChange={onChangeId}
        />
        <input
          type="password"
          value={state.password}
          placeholder="password"
          onChange={onChangePassword}
        />
        <button>{userStore.data ? '로그아웃' : '로그인'}</button>
      </form>
      {userStore.isLogged && `${userStore.data.name}환영합니다.`}
      {userStore.isLoggingIn
        ? '로그인중입니다.'
        : !userStore.data.name && '로그인해주세요'}
      {postStore.posts.length > 0 && `게사판 글수:${postStore.posts.length}`}
    </>
  ))
}

export default App
