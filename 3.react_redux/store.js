const reducer = require('./reducers')
const { createStore, applyMiddleware } = require('redux')
const { composeWithDevTools } = require('redux-devtools-extension')

const initState = {
  user: {
    isLoggingIn: false,
    data: null
  },
  posts: [],
  comments: [],
  favorites: [],
  hisotry: [],
  likes: [],
  followers: []
}

const testMiddleWare = (store) => (next) => (action) => {
  next(action)
}

const thunkMiddleware = (store) => (next) => (action) => {
  console.log('미드', action)
  if (typeof action === 'function') {
    return action(store.dispatch, store.getState)
  } else {
    return next(action)
  }
}

const enhancer = composeWithDevTools(
  applyMiddleware(testMiddleWare, thunkMiddleware)
)

const store = createStore(reducer, initState, enhancer)
store.subscribe(() => {
  console.log('change', store.getState())
})
console.log(store.getState())

module.exports = store
