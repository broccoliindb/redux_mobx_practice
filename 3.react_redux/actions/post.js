const addpost = (data) => {
  return {
    type: 'ADD_POST',
    data
  }
}

module.exports = {
  addpost
}
