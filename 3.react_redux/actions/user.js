const login = (data) => {
  return (dispatch, state) => {
    dispatch(loginRequest(data))
    try {
      setTimeout(() => {
        dispatch(
          loginSuccess({
            userId: 1,
            name: '용범이'
          })
        )
      }, 2000)
    } catch (e) {
      dispatch(loginFailure(data))
    }
  }
}

const loginRequest = (data) => {
  return {
    type: 'LOGIN_REQUEST',
    data
  }
}

const loginSuccess = (data) => {
  return {
    type: 'lOGIN_SUCCESS',
    data
  }
}

const loginFailure = (data) => {
  return {
    type: 'LOGIN_FAIL',
    data
  }
}

const logout = (data) => {
  return {
    type: 'LOGOUT'
  }
}

module.exports = {
  login,
  logout
}
