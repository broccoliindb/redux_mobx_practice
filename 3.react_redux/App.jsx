import React, { useCallback } from 'react'
import { login, logout } from './actions/user'
import { useSelector, useDispatch } from 'react-redux'
const App = () => {
  const isLoggingIn = useSelector((state) => state.user.isLoggingIn)
  const user = useSelector((state) => state.user.data)
  const dispatch = useDispatch()
  const loginHandler = useCallback(() => {
    dispatch(
      login({
        id: 'asdgasfcasdf',
        password: '112341'
      })
    )
  }, [])
  const logoutHandler = useCallback(() => {
    dispatch(logout())
  }, [])
  return (
    <div>
      <button onClick={isLoggingIn ? logoutHandler : loginHandler}>
        {isLoggingIn ? '로그아웃' : '로그인'}
      </button>
      {isLoggingIn
        ? user && user.name
          ? `${user.name}환영`
          : '로그인중입니다.'
        : '로그인해주세요'}
    </div>
  )
}

export default App
