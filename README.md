# REDUX , MobX

## 리덕스, MobX의 사용법과 개념 관련 라이브러리의 대한 연습 및 정리를 위한 소스

ℹ️ 리덕스 MobX는 리액트와 관계없음. 상태관리를 하기위한 방법의 종류로써 있는 라이브러리!!

ℹ️ 리액트는 내부적으로 contextApi를 사용한 상태관리 방법도 있다 하지만 일반적으로 많이 사용하는 상태관리 방법을 익히고 서로 비교하는것도 좋은 학습법!!

- 뷰나 앵귤러 등의 각 프레임워크 나름의 상태관리방식이 있음. 그래서 일반적으로 리덕스는 리액트와 같이 많이 사용된다.
- 부모가 자식 컴포넌트의 스테이트를 변경하려면 보통 함수를 자식에게 내려주고 내려준 함수를 통해 부모의 상태를 변화해준다. 자식의 상태는 변경해줄 수 없다. 할 수 있어도 해선 안된다. **왜냐면 데이터의 변경 방향은 위에서 아래로 단방향으로 일정한 방향을 가지고 설계하는 것이 좋기때문!**
- 참고로 2 way 바인딩은 데이터가 바뀌면 화면도 동시에 바뀌고 화면이 바뀌면 데이터도 자동으로 바뀌게끔 모델링 되어있지만 이 경우 의도치 않은 변경이 트리거되어 발생할 여지가 있음. `모던 웹프레임워크는 따라서 단방향이 더 많다.`

## 리덕스는 왜 쓸까?
- 리덕스는 스테이트를 따로 관리한다.
- 컴포넌트 상호간에 스테이트를 넘나들때 관리를 좀 더 쉽게하기 위해서 사용하는 것이다. 따라서 상호간 관련없는 스테이트의 경우는 리덕스에 포함될 필요가 없다. 
- 리덕스를 사용하면 컴포넌트가 스테이트를 가지고 있을 필요가 없다. **단지 각 개별로 컴포넌트 상태를 관리하는 놈들이 필요하면 리덕스보다는 개별로 setState 혹은 useState를 사용하는 것도 좋다.**

## 리덕스의 장단점
- 👼 장점
  - 리덕스는 스토어라는 곳에 데이터를 보관함. 어디서든 참조가능
  - 규격적인 단방향을 통해 상태값변경
  ```
  스토어 -> 디스패치(액션(객체)) -> 미들웨어 -> 리듀서 -> 스토어에 새로운객체로 대체
  ```
    ![redux-flow](./images/redux-flow.png)
  - devtool을 통한 히스토리를 통해 에러를 찾기 편하다.

- 👿 단점
  - 액션을 정의해줘야한다.
  - 스테이트를 매번 새로 생성해야한다.-> immer로 완화가능
  - 코드가 길어질 수 있다.

## 리덕스 미들웨어
디스패치와 리듀서 사이에서 동작하는 것, 로깅도 있을 수 있고 비동기 액션을 제어해주기위해서 아래 두개의 미들웨어를 쓸 수도 있다. 

- redux thunk: 비동기 제어할 때 사용(액션이 함수인경우) 
- redux saga: 비동기제어, 제너레이터사용

## 리액트 리덕스
리액트소스에서 dispatch와 state를 사용함 
```
npm i react-redux
```

함수형의경우 아래의 훅을 사용함.
- useDispatch: dispatch할때 사용하는 훅
- useSelector: store에 있는 state를 선택할때 사용하는 훅

- 주의사항
  - 인풋에서 onChange, value와 바인딩되는 것은 가능하면 트리거 될때마다 디스패치하지말고 submit 혹은 blur때 한번에 처리되도록하자
  - 컴포넌트 하나에만 관련된 비동기의 경우는 내부에서 함수로 처리하자 굳이 비동기 action을 할 필요는 없다.
  - useSelector를 사용할 때 객체단위로하게 되면 성능에 안좋을 수 있다. 그런경우 reselector를 사용하자

## immer
리듀서에서는 상태값이 변경될때 매번 새로 생성을 해줘야한다. 그럼 매번 스프레드를 사용해줘야하는데 직관점이 떨어지고 코드의 양이 길어진다 따라서 해당 단점을 해결하고자 immer를 사용한다.

```
const { produce } = require('immer')

...
const reducer = (prevState = initState, action) => {
  return produce(prevState, (draft) => {
    switch (action.type) {
      case 'LOGIN':
        draft.isLoggingIn = true
        break
      default:
        break
    }
  })
}
```

state에 값을 직접 넣는 등 불변성이 끊길경우 return에 state를 넣어준다.

## redux-devtools-extension

https://www.npmjs.com/package/redux-devtools-extension#usage

구글 크롬에 리덕스 익스텐션을 설치한후 소스에 해당 부분을 아래와 같이 설치한 후 연결해주면 리덕스상태변화의 히스토리를 확인할 수 있다.

아래 패키지 설치후 미들웨어연결
```
npm i redux-devtools-extension -D
```

```
//store.js
const { composeWithDevTools } = require('redux-devtools-extension')

...
const enhancer = process.env.NODE_ENV === 'production'
    ? compose(applyMiddleware(testMiddleWare, thunkMiddleware))
    : composeWithDevTools(applyMiddleware(testMiddleWare, thunkMiddleware))

...

const enhancer = composeWithDevTools(
  applyMiddleware(testMiddleWare, thunkMiddleware)
)

const store = createStore(reducer, initState, enhancer)
```

## thunk
리덕스는 비동기 처리가 없다. 그래서 비동기 처리를 해주는 미들웨어 라이브러리가 몇가지 있는데 그중에 하나이다. 로직이 짧고 간단해서 직접 구현해도 된다.

디스패치의 액션을 함수로 해서 해당 액션에 여러개의 디스패치를 담아 여러 액션을 실행할 수 있도록 해줌.
단 비동기 액션이 axios로 덕지덕지 지저분해보이게 될 수 있음.

```
const thunkMiddleware = (store) => (next) => (action) => {
  if (typeof action === 'function') {
    return action(store.dispatch, store.getState)
  } else {
    return next(action)
  }
}
```

## saga
- 제너레이터: 함수 실행을 중간에 멈출 수 있고 원할 때 재개할 수 있어서 편함
- all: 배열로 인자를 받아서 그 내부의 인자들을 한번에 실행한다.
- fork: 비동기 함수 호출
- call: 동기 함수 호출
- take: 매개변수로 전달된 액션이 올때까지 블락된 상태로 기다린다. 일회성으로만 핸들링됨
- put: redux의 dispatch 같은 역할
- race: 이펙트들을 동시실행시키고 먼저완료된애가 있으면 다른 이펙트를 종료한다.
- select: state에서 데이터를 꺼내오기위한 함수
- takeLatest: 마지막에 실행된 액션에 대해서만 핸들러실행. 단 이미 실행완료된 액션에 대해서는 해당안되고, 동시에 진행중인것들에 대해서만 해당됨. 응답에대해서만 취소되는것. 백엔드서버로 요청은 다 들어감....??
- takeEvery: 캐치된 액션 모두에 대해서 핸들러 실행함

## 리덕스 툴킷
리덕스 팀에서 자주 사용하는 라이브러리들을 모아 툴킷을 만듦
- immer 내장
- redux-devtools-extension 내장
- thunk 내장
- redux 내장

```
// thunk, immer, devtools 다 지우면 됨.
const { configureStore } = require('@reduxjs/toolkit')
...
const store = configureStore(reducer)
```

- createAsyncThunk
- createSlice
보통 이 두개를 주로 사용한다.

## MobX
![mobx-flow](./images/mobx-flow.png)

- 데코레이터 사용 (5버전까지는 사용함. 표준 아님) 
  - class, export는 감쌀 수 있으나 옵션이 변경된 경우에 데코레이터를 사용못하는 경우도 있음.
  - 일반 객체리터럴에도 못씀. 이경우엔 실제로 함수(데코레이터는 함수임) 로 감싸줘야함. 

- observable로 감싼 객체를 액션을 통해서 상태변화시키는 라이브러리, reduce, dispatch는 없다.
- 자유도가 높다. redux의 액션은 리듀서에 종속적이 되기 때문에 두개이상의 리듀서의 나뉜 액션을 하나의 액션단위로 만들 수가 없다. **하지만 MobX는 상반된 두개의 상태값을 하나의 action으로 진행가능하다.**
- state를 분리할 수 있다. redux는 initialState를 하나로 묶어야하고 그 묶은 단위단위의 state별로 reducer를 나눌수는 있으나 결국 다시 ininState의 형태로 함치게 된다.  
- observable : state객체를 감싸는 모듈, 단 observable은 원시값으로 distructuring하면 observable이 깨진다.
- immer가 내장되어있어 state에 그냥 바로 바꿔도 상태값 변경한다. 이런 행위를 action이라고 할 수 있는데, 시각적으로 차이가 없으므로 runInAction 등의 함수로 감쌀수도 있다.
- runInAction : 함수에 한 단위에 감싸면 한단위로 상태가 변경된다.
- action: action에 감싸이면 나중에 호출해서 상태를 변경할 수 있다.
- autorun: 상태가 변경될때마다 자동으로 호출된다.
- reaction: 첫번째 인자의 상태값이 변경될때마다 두번째인자가 호출된다.
- computed: 캐싱
- devtool이 있기는 하다. 하지만 리덕스처럼 보기 편하지 않다.

## Mobx-react
observer는 이곳에 있다. 
useAsObservableSource 의 목적은 props를 Observable처럼 해주는데 목적이 있다.

## MobX State Tree
Mobx가 너무 자유도가 크기 때문에 Mobx에 조금 틀을 잡아주는 라이브러리. 
