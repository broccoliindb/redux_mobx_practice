const { createStore } = require('redux')

const login = (data) => {
  return {
    type: 'LOGIN',
    data
  }
}

const logout = (data) => {
  return {
    type: 'LOGOUT'
  }
}

const addpost = (data) => {
  return {
    type: 'ADD_POST',
    data
  }
}

const reducer = (prevState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...prevState,
        user: action.data
      }
    case 'LOGOUT':
      return {
        ...prevState,
        user: null
      }
    case 'ADD_POST':
      return {
        ...prevState,
        posts: [...prevState.posts, action.data]
      }
    default:
      return prevState
  }
}

const initState = {
  user: null,
  posts: []
}

const store = createStore(reducer, initState)
store.subscribe(() => {
  console.log('change')
})
console.log(store.getState())
store.dispatch(login({ id: 1, user: 'a', password: '1111', admin: true }))
console.log(store.getState())
store.dispatch(addpost({ id: 1, userId: 1, content: 'first post' }))
console.log(store.getState())
store.dispatch(addpost({ id: 2, userId: 1, content: 'second post' }))
console.log(store.getState())
store.dispatch(logout())
console.log(store.getState())
