import React, { Component } from 'react'
import { login, logout } from './actions/user'
import { connect } from 'react-redux'
class App extends Component {
  loginHandler = () => {
    this.props.dispatchLogin({
      id: 'asdgasfcasdf',
      password: '112341'
    })
  }
  logoutHandler = () => {
    this.props.dispatchLogout()
  }
  render() {
    const { user } = this.props
    return (
      <div>
        <button
          onClick={user.isLoggingIn ? this.logoutHandler : this.loginHandler}
        >
          {user.isLoggingIn ? '로그아웃' : '로그인'}
        </button>
        {user.isLoggingIn
          ? user && user.data && user.data.name
            ? `${user.data.name}환영`
            : '로그인중입니다.'
          : '로그인해주세요'}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log('state', state)
  return {
    user: state.user,
    posts: state.posts
  }
} // reselect사용해야 할경우가 많음. 훅을 사용시에는 그럴필요 없음.

const mapDispatchToProps = (dispatch) => ({
  dispatchLogin: (data) => dispatch(login(data)),
  dispatchLogout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
