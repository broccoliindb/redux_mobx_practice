const { produce } = require('immer')

const initialState = {
  isLoggingIn: false,
  data: {}
}
const userReducer = (prevState = initialState, action) => {
  return produce(prevState, (draft) => {
    switch (action.type) {
      case 'LOGIN':
        draft.isLoggingIn = true
        draft.data = action.data
        break
      case 'LOGOUT':
        draft.isLoggingIn = false
        draft.data = null
        break
      case 'LOGIN_REQUEST':
        draft.isLoggingIn = true
        draft.data = action.data
        break
      case 'lOGIN_SUCCESS':
        draft.isLoggingIn = true
        draft.data = action.data
        break
      default:
        break
    }
  })
}

module.exports = userReducer
